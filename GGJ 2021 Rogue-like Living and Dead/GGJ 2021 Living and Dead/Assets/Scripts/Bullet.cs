using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Bullet : MonoBehaviour
{
    public enum Team
    {
        Player, Enemies
    }
    
    private float bulletSpeed = 5f;
    public int damage = 1;
    public Team team = Team.Player;

    private void Update()
    {
        transform.Translate(Vector3.right * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null && collision.gameObject.tag != "Bullet" && collision.gameObject.tag != "Item")
        {
            switch (team)
            {
                case Team.Player:
                    if (collision.gameObject.tag == "Enemy")
                    {
                        collision.GetComponent<CharacterController>().Damage(damage);
                        Destroy(gameObject);
                    }
                    break;
                case Team.Enemies:
                    if (collision.gameObject.tag == "Player")
                    {
                        collision.GetComponent<CharacterController>().Damage(damage);
                        Destroy(gameObject);
                    }
                    break;
            }

            if(collision.gameObject.tag != "Enemy" && collision.gameObject.tag != "Player")
            {
                Destroy(gameObject);
            }

        }
        Destroy(gameObject, 4f);
    }

    public void SetDamage(int damage)
    {
        this.damage = damage;
    }

    public void SetBulletSpeed(float bulletSpeed)
    {
        this.bulletSpeed = bulletSpeed;
    }

    public int GetDamage()
    {
        return damage;
    }

    public float GetBulletSpeed()
    {
        return bulletSpeed;
    }
}
