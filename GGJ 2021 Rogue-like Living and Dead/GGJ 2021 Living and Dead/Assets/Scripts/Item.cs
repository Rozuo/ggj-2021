using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewInventoryItem", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    [Header("General Settings")]
    public ItemID id;
    public string itemName = "Unknown Item";
    public string itemDescription = "Unknown Description";
    public Sprite itemImage;

    [Header("Player Statistics")]
    public int playerHealth;
    public int playerHealing;
    public float playerSpeed;

    [Header("Weapon Statistics")]
    public int weaponDamage;
    public int weaponSize;
    public float weaponRof; //negative values increase rof
    public int weaponBullets;
    public float weaponSpeed; 
}

public enum ItemID
{
    itemHealSmall,
    itemHealMed,
    itemHealLarge,
    itemMaxHealthSmall,
    itemMaxSpeedSmall,
    itemWpnDamageSmall,
    itemWpnSizeSmall,
    itemWpnRofSmall,
    itemWpnBulletsSmall,
    itemWpnSpeedSmall,
    total
}