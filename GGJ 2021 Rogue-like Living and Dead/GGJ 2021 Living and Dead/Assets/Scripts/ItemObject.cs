using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObject : MonoBehaviour
{
    public Item itemid;

    private Inventory invObj;
    private BoxCollider2D boxCollide;
    private IEnumerator pickupCoroutine;
    private GameObject playerCollider;
    
    // Start is called before the first frame update
    void Start()
    {        
        if (!GameObject.Find("Inventory"))
        {
            Debug.LogWarning(gameObject.name + " failed to find the Inventory object. Make sure to add the Inventory prefab to your Scene!");
            gameObject.SetActive(false);
        }
        else
        {
            invObj = GameObject.Find("Inventory").GetComponent<Inventory>();
        }

        boxCollide = gameObject.GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        if(boxCollide.enabled == false)
        {
            Vector3 colliderPos = playerCollider.transform.position;
            gameObject.transform.position = new Vector3(colliderPos.x, colliderPos.y + 1, colliderPos.z - 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            //call inventory, add item to inventory, destroy object          
            switch(itemid.id)
            {
                case ItemID.itemHealSmall: //healing items don't get added
                case ItemID.itemHealMed:
                case ItemID.itemHealLarge:
                    break;
                default:
                    invObj.AddItem(itemid, 1);
                    break;

            }            
                
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            
            playerCollider = collision.gameObject;
            UpdatePlayerStats(playerCollider.GetComponent<PlayerController>().stats, itemid);           

            pickupCoroutine = OnPickupTimer(1.5f);
            StartCoroutine(pickupCoroutine);

            if (invObj.invUI)
            {
                invObj.invUI.RefreshInventory();
                invObj.invUI.SetTimedDescription(itemid.itemName + ": " + itemid.itemDescription);
            }
        }       
    }

    private void UpdatePlayerStats(CharacterStats stat, Item itm)
    {
        switch(itemid.id)
        {
            case ItemID.itemHealSmall:
            case ItemID.itemHealMed:
            case ItemID.itemHealLarge:
                // Player Healing
                stat.SetHp(Mathf.Min(stat.GetHp() + itm.playerHealing, stat.GetMaxHp()));
                break;
            case ItemID.itemMaxHealthSmall:
                stat.SetMaxHp(stat.GetMaxHp() + itm.playerHealth);
                stat.SetHp(Mathf.Min(stat.GetHp() + itm.playerHealing, stat.GetMaxHp()));
                break;
            case ItemID.itemMaxSpeedSmall:
                stat.SetMoveSpeed(stat.GetMoveSpeed() + itm.playerSpeed);
                break;
            case ItemID.itemWpnDamageSmall:
                stat.SetWeaponDamage(stat.GetWeaponDamage() + itm.weaponDamage);
                break;
            case ItemID.itemWpnBulletsSmall:
                stat.SetWeaponBullets(stat.GetWeaponBullets() + itm.weaponBullets);
                break;
            case ItemID.itemWpnRofSmall:
                stat.SetWeaponRof(stat.GetWeaponRof() + itm.weaponRof);
                break;
            case ItemID.itemWpnSizeSmall:
                stat.SetWeaponSize(stat.GetWeaponSize() + itm.weaponSize);
                break;                        
            case ItemID.itemWpnSpeedSmall:
                stat.SetWeaponSpeed(stat.GetWeaponSpeed() + itm.weaponSpeed);
                break;
            default:
                Debug.LogWarning("Item has no specified id");
                break;
        }      
    }

    private IEnumerator OnPickupTimer(float time)
    {
        yield return new WaitForSeconds(time);        
        Destroy(gameObject);
    }
}
