using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowAwayDebug : MonoBehaviour
{

    public GameObject bullet;

    public float lastFired = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lastFired < Time.time)
        {
            GameObject gO = Instantiate(bullet, transform.position, Quaternion.Euler(0, 0, 90));
            lastFired = Time.time + 1.0f;
        }
    }
}
