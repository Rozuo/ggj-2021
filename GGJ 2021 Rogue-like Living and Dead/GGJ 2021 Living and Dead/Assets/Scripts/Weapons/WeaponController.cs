﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController: MonoBehaviour
{
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] int ammo = -1;
    [SerializeField] GameObject fireAudio;
    AudioSource FireSound { get { return fireAudio?.GetComponent<AudioSource>(); } }

    public void Fire(Vector3 origin, float directionDegrees, int damageFactor, float speedFactor, int numBullets, float spread = 15f)
    {
        float [] angles = new float[numBullets];
        angles[0] = directionDegrees;
        if(numBullets > 1)
        {
            spread = 15f; // remove if spread becomes a stat
            float spreadAngle = spread / numBullets;

            float rZSpawnAngle = directionDegrees + spread; // right of zero
            if(numBullets > 2)
            {
                float lZSpawnAngle = directionDegrees - spread; // left of zero
            
                for(int i = 1; i < numBullets; i+=2, rZSpawnAngle -= spreadAngle, lZSpawnAngle += spreadAngle) // might be flawed test first
                {
                    angles[i] = rZSpawnAngle;
                    if((i + 1) >= numBullets)
                    {
                        break;
                    }
                    angles[i+1] = lZSpawnAngle;
                }
            }
            else
            {
                for(int i = 1; i < numBullets; i++, rZSpawnAngle -= spreadAngle)
                {
                    angles[i] = rZSpawnAngle;
                }
            }
        }
        for (int i = 0; i < numBullets && ammo != 0; i++, ammo--)
        {
            var bulletObj = Object.Instantiate(bulletPrefab, origin, Quaternion.Euler(0, 0, angles[i]));
            var bullet = bulletObj.GetComponent<Bullet>();
            bullet.SetBulletSpeed(bullet.GetBulletSpeed() * speedFactor);
            bullet.SetDamage(bullet.GetDamage() * damageFactor);
        }
        FireSound?.Play();
    }

    public void Fire(Vector3 origin, Vector2 direction, int damageFactor, float speedFactor, int numBullets, float spread = 0f)
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Fire(origin, angle, damageFactor, speedFactor, numBullets);
    }

    public void FireAt(Vector3 origin, Vector2 targetPos, int damageFactor, float speedFactor, int numBullets)
    {
        Fire(origin, new Vector2(targetPos.x - origin.x, targetPos.y - origin.y), damageFactor, speedFactor, numBullets);
    }
}
