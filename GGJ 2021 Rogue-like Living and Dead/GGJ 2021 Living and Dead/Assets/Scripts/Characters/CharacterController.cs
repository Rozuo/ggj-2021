using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
abstract public class CharacterController : MonoBehaviour
{
    public enum LivingStates
    {
        Alive, unDead, Dead
    }

    public CharacterStats stats;

    [SerializeField]
    protected LivingStates livingState = LivingStates.Alive;

    protected Vector2 movementInput;
    public GameObject weapon;
    protected float nextFireTime = 0f;

    protected WeaponController WeaponController { get { return weapon.GetComponent<WeaponController>(); } }

    public LivingStates GetLivingState()
    {
        return livingState;
    }

    public void MakeUndead()
    {
        livingState = LivingStates.unDead;
    }

    public void Revived()
    {
        livingState = LivingStates.Alive;
    }

    public virtual void Heal(int amount)
    {
        int max = stats.GetMaxHp();
        int hp = stats.GetHp();
        int totalAmount = (hp + amount) >= max ? max : hp + amount;
        stats.SetHp(totalAmount);
    }

    public abstract void Damage(int amount);
}
