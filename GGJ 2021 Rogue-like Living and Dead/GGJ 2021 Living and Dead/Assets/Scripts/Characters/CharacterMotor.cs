using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterMotor
{
    protected float moveSpeed;
    
    protected Rigidbody2D rb2d;
    protected Animator anim;
    
    public abstract void move(Vector2 move, bool forceBased = false);

}
