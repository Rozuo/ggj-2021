using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterStats 
{
    [Header("Character Statistics")]
    [SerializeField]
    protected int maxHp = 100;
    [SerializeField]
    protected int hp = 100;
    [SerializeField]
    protected int udHp = 1;
    [SerializeField]
    protected float moveSpeed = 2f;

    [Header("Weapon Stats")]
    [SerializeField]
    protected int weaponDamage = 10;
    [SerializeField]
    protected int weaponSize = 1;
    [SerializeField]
    protected int weaponBullets = 1;
    [SerializeField]
    protected float weaponRof = 1;
    [SerializeField]
    protected float weaponSpeed = 1;

    public CharacterStats(int maxHp, float moveSpeed, int weaponDamage, 
        int weaponSize, int weaponBullets, float weaponRof, float weaponSpeed, int udHp = 1)
    {
        this.maxHp = maxHp;
        hp = maxHp;
        this.udHp = udHp;
        this.moveSpeed = moveSpeed;
        this.weaponDamage = weaponDamage;
        this.weaponSize = weaponSize;
        this.weaponBullets = weaponBullets;
        this.weaponRof = weaponRof;
        this.weaponSpeed = weaponSpeed;
    }

    // Setters
    public void SetMaxHp(int maxHp)
    {
        this.maxHp = maxHp;
    }
    
    public void SetHp(int hp)
    {
        this.hp = hp;
    }
    
    public void SetUdHp(int udHp)
    {
        this.udHp = udHp;
    }

    public void SetMoveSpeed(float moveSpeed)
    {
        this.moveSpeed = moveSpeed;
    }
    
    public void SetWeaponDamage(int weaponDamage)
    {
        this.weaponDamage = weaponDamage;
    }
    
    public void SetWeaponSize(int weaponSize)
    {
        this.weaponSize = weaponSize;
    }
    
    public void SetWeaponBullets(int weaponBullets)
    {
        this.weaponBullets = weaponBullets;
    }
    
    public void SetWeaponRof(float weaponRof)
    {
        this.weaponRof = weaponRof;
    }
    
    public void SetWeaponSpeed(float weaponSpeed)
    {
        this.weaponSpeed = weaponSpeed;
    }
    
    // Getters
    public int GetMaxHp()
    {
        return maxHp;
    }
    
    public int GetHp()
    {
        return hp;
    }
    
    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public void GetUdHp(int udHp)
    {
        this.udHp = udHp;
    }

    public int GetWeaponDamage()
    {
        return weaponDamage;
    }
    
    public int GetWeaponSize()
    {
        return weaponSize;
    }
    
    public int GetWeaponBullets()
    {
        return weaponSize;
    }
    
    public float GetWeaponRof()
    {
        return weaponRof;
    }
    
    public float GetWeaponSpeed()
    {
        return weaponSpeed;
    }
}
