using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Controls the basic functionality of the player.
/// </summary>
/// 
/// Author: Rozario (Ross) Beaudin (RB)
/// 
public class PlayerController : CharacterController
{
    private GameManager gameManager;
    private bool gamepadActive = false;

    private Vector2 firePos;
    private PlayerMotor motor;

    private float invincTime = 0.6f;
    private float lastDamageTime = 0;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        stats = new CharacterStats(100, 10f, 10, 1, 1, 0.4f, 5);
        motor = new PlayerMotor(stats.GetMoveSpeed(), GetComponent<Rigidbody2D>(), GetComponent<Animator>());
        Debug.Log("maxhp " + stats.GetMaxHp());
        if(Input.GetJoystickNames().Length >= 1)
            gamepadActive = true;
    }

    void Update()
    {
        //Debug.Log("maxhp " + stats.GetMaxHp());
        movementInput.x = Input.GetAxis("Horizontal");
        movementInput.y = Input.GetAxis("Vertical");

        bool shouldFire = false;
        
        if (gamepadActive)
        {
            firePos.x = Input.GetAxis("RightHorizontal");
            firePos.y = Input.GetAxis("RightVertical");
            if(firePos.magnitude > 0 && nextFireTime < Time.time)
            {
                shouldFire = true;
                nextFireTime = Time.time + stats.GetWeaponRof();
            }
        }
        if (Input.GetAxis("Fire1") > 0 && nextFireTime < Time.time)
        {
            shouldFire = true;
            firePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            firePos = new Vector2(firePos.x - transform.position.x, firePos.y - transform.position.y);
            nextFireTime = Time.time + stats.GetWeaponRof();
        }

        if(shouldFire)
        {
            WeaponController.Fire(WeaponController.transform.position, firePos, stats.GetWeaponDamage(), stats.GetWeaponSpeed(), stats.GetWeaponBullets());
            nextFireTime = Time.time + stats.GetWeaponRof();
        }
    }

    private void FixedUpdate()
    {
        motor.move(movementInput, livingState == LivingStates.unDead);
    }

    public void ZeroForce()
    {
        motor.ZeroForce();
    }

    public override void Damage(int amount)
    {
        if (Time.time - lastDamageTime < invincTime) return;

        Color c = Color.red;
        c.a = 0.5f;
        PlayerHUD.Instance.DoCameraFlash(c);
        lastDamageTime = Time.time;

        // lose half items
        int currentHp = stats.GetHp();
        if (currentHp > 0)
        {
            currentHp -= amount;
            stats.SetHp(currentHp);
        }
        
        //Debug.Log(gameObject.name + " hp is now " + stats.GetHp());
        if (currentHp <= 0)
        {
            switch (livingState)
            {
                case LivingStates.unDead:
                    livingState = LivingStates.Dead;
                    PlayerPermaDead();
                    break;
                case LivingStates.Alive:
                    livingState = LivingStates.unDead;
                    gameManager.PlayerDied();
                    break;
                case LivingStates.Dead:
                default:
                    break;
            }
        }
    }

    public void PlayerPermaDead()
    {
        gameManager.PlayerPermaDeath();
        //Destroy(this.gameObject);
        gameObject.SetActive(false);
    }
}
