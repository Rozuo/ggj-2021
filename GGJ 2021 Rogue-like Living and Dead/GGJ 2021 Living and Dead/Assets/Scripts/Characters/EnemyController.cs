using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : CharacterController
{
    [SerializeField] GameObject player;

    EnemyAI ai;
    Vector2 shootTarget;

    private PlayerMotor motor;
    private ItemDrop drops;

    public float MovementSpeed = 4;
    public float fireDelay = 0.4f;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //manager = gameObject.Find;
    }

    // Start is called before the first frame update
    void Start()
    {
        ai = new NaiveEnemyAI()
        {
            enemy = gameObject,
            player = player
        };

        motor = new PlayerMotor(MovementSpeed, GetComponent<Rigidbody2D>(), GetComponent<Animator>());
        drops = gameObject.GetComponent<ItemDrop>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Play)
        {
            if (livingState == LivingStates.Dead)
            {
                return;
            }

            var directive = ai.GetDirective();

            if (directive.MoveTarget.HasValue)
            {
                movementInput.x = directive.MoveTarget.Value.x - transform.position.x;
                movementInput.y = directive.MoveTarget.Value.y - transform.position.y;
            }
            else
            {
                movementInput = Vector2.zero;
            }

            if (directive.ShootTarget.HasValue && nextFireTime < Time.time)
            {
                shootTarget = directive.ShootTarget.Value;
                WeaponController.FireAt(transform.position, shootTarget, 5, 1f, 1);
                nextFireTime = Time.time + fireDelay;
            }
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.Instance.CurrentState == GameManager.GameState.Play)
        {
            motor.move(movementInput, livingState == LivingStates.unDead);
        }
    }

    public override void Damage(int amount)
    {
        int currentHp = stats.GetHp();
        currentHp -= amount;
        stats.SetHp(currentHp);
        //Debug.Log(gameObject.name + " hp is now " + stats.GetHp());
        if (currentHp <= 0)
        {
            switch (livingState)
            {
                case LivingStates.unDead:
                    GameManager.Instance.KilledUndead(this);
                    break;
                case LivingStates.Alive:
                    GameManager.Instance.KilledEnemy(this);
                    drops.DropItems();
                    break;
                case LivingStates.Dead:
                default:
                    break;
            }

            livingState = LivingStates.Dead;
            Destroy(gameObject);
        }

    }
}
