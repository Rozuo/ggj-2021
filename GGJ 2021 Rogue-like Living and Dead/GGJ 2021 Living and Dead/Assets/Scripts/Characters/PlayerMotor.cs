using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls all physics of player
/// </summary>
/// 
/// Author: Rozario (Ross) Beaudin (RB)
/// 
public class PlayerMotor : CharacterMotor
{
    public PlayerMotor(float moveSpeed, Rigidbody2D rb2d, Animator anim)
    {
        this.moveSpeed = moveSpeed;
        this.rb2d = rb2d;
        this.anim = anim;
    }

    public override void move(Vector2 move, bool forceBased = false)
    {
        if(move.magnitude > 1)
        {
            move.Normalize();
        }

        if (!forceBased)
        {
            //rb2d.bodyType = RigidbodyType2D.Kinematic;
            rb2d.MovePosition(rb2d.position + move * moveSpeed * Time.fixedDeltaTime);
        }
        else
        {
            //rb2d.bodyType = RigidbodyType2D.Dynamic;
            rb2d.AddForce(move * moveSpeed);
        } 

        if (anim == null)
            return;
        int direction = (3 - Mathf.RoundToInt(Mathf.Atan2(move.y, move.x) * Mathf.Rad2Deg / 90f)) % 4;
        anim.SetBool("IsWalking", move.magnitude > 0 && !forceBased);
        anim.SetInteger("Direction", direction);
    }

    public void ZeroForce()
    {
        rb2d.velocity = Vector2.zero;
        rb2d.angularVelocity = 0;
    }
}
