using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DunGen;
using System.Threading.Tasks;
using UnityEngine.Jobs;

public class CameraTransition : MonoBehaviour
{

    protected bool isDoingTransition = false;    
    protected DungeonGenerator generator;
    protected List<Tile> allTiles;

    public static CameraTransition Instance;

    public bool IsReady { get; protected set; }
    public Tile CurrentTile { get; protected set; }
    public Tile LastTile { get; protected set; }

    [SerializeField] private Transform playerTransform;
    [SerializeField] private Camera playerCamera;

    protected virtual void Awake()
    {
        Instance = this;

        var runtimeDungeon = FindObjectOfType<RuntimeDungeon>();

        if (runtimeDungeon != null)
        {
            generator = runtimeDungeon.Generator;
            generator.OnGenerationStatusChanged += OnDungeonGenerationStatusChanged;

            if (generator.Status == GenerationStatus.Complete)
                SetDungeon(generator.CurrentDungeon);
        }
    }

    protected virtual void OnDungeonGenerationStatusChanged(DungeonGenerator generator, GenerationStatus status)
    {
        if (status == GenerationStatus.Complete)
            SetDungeon(generator.CurrentDungeon);
        else if (status == GenerationStatus.Failed)
            ClearDungeon();
    }

    protected virtual void LateUpdate()
    {
        if (!IsReady || isDoingTransition)
            return;

        Transform target = (playerTransform != null) ? playerTransform : transform;
        bool hasPositionChanged = false;

        if (CurrentTile == null || !CurrentTile.Bounds.Contains(target.position))
        {
            foreach (var tile in allTiles)
            {
                if (tile == null)
                    continue;

                if (tile.Bounds.Contains(target.position))
                {
                    if (tile != CurrentTile)
                    {
                        LastTile = CurrentTile;
                        CurrentTile = tile;
                        hasPositionChanged = true;
                        break;
                    }
                }
            }
            if (CurrentTile != null && hasPositionChanged)
            {
                DoCameraTransition(CurrentTile, 0.5f);
            }
        }
    }

    private async void DoCameraTransition(Tile targetTile, float duration)
    {
        if(targetTile == null)
        {
            Debug.LogError("Can't transition to null tile");
        }
        GameManager.Instance.SetState(GameManager.GameState.Transition);
        if(LastTile != null) LastTile.GetComponent<RoomData>().RoomExited();
        isDoingTransition = true;
        Time.timeScale = 0;
        await cameraTransitionTask(targetTile, duration);
        Time.timeScale = 1;
        isDoingTransition = false;
        //targetTile.GetComponent<RoomData>().StartRoom();
        await Task.Delay(500);
        GameManager.Instance.SetState(GameManager.GameState.Play);
    }

    private async Task<bool> cameraTransitionTask(Tile targetTile, float duration)
    {
        RoomData room = targetTile.GetComponent<RoomData>();
        room.SetSkin(MapManager.Instance.CurrentState);

        float elapsedTime = 0;
        Vector3 oldPos = playerCamera.transform.position;
        Vector3 targetPos = targetTile.transform.position;
        targetPos.z = oldPos.z;

        while (elapsedTime < duration)
        {
            playerCamera.transform.position = Vector3.Lerp(oldPos, targetPos, elapsedTime / duration);
            await new WaitForUpdate();
            elapsedTime += Time.unscaledDeltaTime;
            //Debug.Log($"elapsedTime = {elapsedTime}, unscaledDelta = {Time.unscaledDeltaTime}");
        }
        playerCamera.transform.position = targetPos;

        return true;
    }

    public void SetDungeon(Dungeon dungeon)
    {
        if (IsReady)
            ClearDungeon();

        if (dungeon == null)
            return;

        allTiles = new List<Tile>(dungeon.AllTiles);

        CurrentTile = dungeon.MainPathTiles[0];

        int count = 0;
        foreach(Tile room in dungeon.AllTiles)
        {
            room.name = $"Room{count}";
            count++;
        }

        IsReady = true;
        isDoingTransition = true;
        DoCameraTransition(CurrentTile, 0f);
    }

    protected virtual void ClearDungeon()
    {
        IsReady = false;
    }
}
