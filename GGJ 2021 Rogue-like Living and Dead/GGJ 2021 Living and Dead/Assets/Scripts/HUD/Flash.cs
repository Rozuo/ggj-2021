using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Flash : MonoBehaviour
{
    ///////////////////////////////////////////////////
    public float flashTimelength = .2f;
    public bool doCameraFlash = false;
    public Color defaultFlashColor;

    ///////////////////////////////////////////////////
    private Image flashImage;
    private float startTime;
    private bool flashing = false;
    private Color initialColor;

    ///////////////////////////////////////////////////
    void Start()
    {
        flashImage = GetComponent<Image>();
        initialColor = flashImage.color;
        initialColor.a = 0.0f;
        flashImage.color = initialColor;
    }

    ///////////////////////////////////////////////////
    void Update()
    {
        if (doCameraFlash && !flashing)
        {
            Debug.Log("Starting flash routine");
            CameraFlash();
        }
        else
        {
            doCameraFlash = false;
        }
    }

    public void CameraFlash()
    {
        CameraFlash(defaultFlashColor, flashTimelength);
    }

    ///////////////////////////////////////////////////
    public void CameraFlash(Color flashColor, float duration)
    {
        if (flashing) return;
        // start time to fade over time
        startTime = Time.time;

        // so we can flash again
        doCameraFlash = false;

        // flash image start color
        flashImage.color = flashColor;

        // flag we are flashing so user can't do 2 of them
        flashing = true;

        StartCoroutine(FlashCoroutine(flashColor, duration));
    }

    ///////////////////////////////////////////////////
    IEnumerator FlashCoroutine(Color flashColor, float duration)
    {
        bool done = false;

        while (!done)
        {
            float perc;
            Color col = flashImage.color;

            perc = Time.time - startTime;
            perc = perc / duration;

            if (perc > 1.0f)
            {
                perc = 1.0f;
                done = true;
            }

            flashImage.color = Color.Lerp(flashColor, initialColor, perc);

            yield return null;
        }

        flashing = false;

        yield break;
    }
}