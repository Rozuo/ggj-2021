using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD : MonoBehaviour
{
    [SerializeField] GameObject healthBar;
    [SerializeField] GameObject player;
    [SerializeField] GameObject map;
    [SerializeField] GameObject darkener;

    PlayerController PlayerController { get { return player.GetComponent<PlayerController>(); } }
    HealthBarUI HealthBarUI { get { return healthBar.GetComponent<HealthBarUI>(); } }
    CharacterStats Stats { get { return PlayerController.stats; } }
    Flash cameraFlash { get { return GetComponentInChildren<Flash>(); } }

    public static PlayerHUD Instance;

    private void Awake()
    {
        Instance = this;
        ShowMap(false);
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        HealthBarUI.updateUI((float)Stats.GetHp() / Stats.GetMaxHp());
    }

    public void DoCameraFlash(Color flashColor, float duration = 0.2f)
    {
        cameraFlash.CameraFlash(flashColor, duration);
    }

    public void ShowMap(bool visible)
    {
        darkener.SetActive(visible);
        map.SetActive(visible);
    }
}
