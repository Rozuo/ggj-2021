using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DunGen;

[RequireComponent(typeof(Camera))]
public class MapHUD : MonoBehaviour
{
    private Camera cam;
    public float padding = 2;

    private float oldPadding = 0;

    private Dungeon dungeon;

    private void Awake()
    {
        cam = GetComponent<Camera>();
        var runtimeDungeon = FindObjectOfType<RuntimeDungeon>();

        if (runtimeDungeon != null)
        {
            DungeonGenerator generator = runtimeDungeon.Generator;
            generator.OnGenerationStatusChanged += OnDungeonGenerationStatusChanged;

            if (generator.Status == GenerationStatus.Complete)
                SetDungeon(generator.CurrentDungeon);
        }
        oldPadding = padding;
    }

    private void Update()
    {
        if(padding != oldPadding)
        {
            RecenterMap();
        }
        oldPadding = padding;
    }

    protected virtual void OnDungeonGenerationStatusChanged(DungeonGenerator generator, GenerationStatus status)
    {
        if (status == GenerationStatus.Complete)
            SetDungeon(generator.CurrentDungeon);
        else if (status == GenerationStatus.Failed)
            ClearDungeon();
    }

    private void SetDungeon(Dungeon dungeon)
    {
        this.dungeon = dungeon;
        RecenterMap();
    }

    private void ClearDungeon()
    {
        this.dungeon = null;
    }

    public void RecenterMap()
    {
        Bounds bounds = dungeon.Bounds;
        Vector3 camPos = bounds.center;

        camPos.z = -100;
        cam.transform.position = camPos;

        if (bounds.extents.y >= bounds.extents.x)
        {
            cam.orthographicSize = bounds.extents.y + padding;
        }
        else
        {
            float aspect = Screen.currentResolution.width / Screen.currentResolution.height;
            cam.orthographicSize = (bounds.extents.x / aspect) + padding;
        }
    }
}
