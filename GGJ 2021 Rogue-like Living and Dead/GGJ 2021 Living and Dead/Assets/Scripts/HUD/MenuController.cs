using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*this script is to control the main menu behaviour*/
public class MenuController : MonoBehaviour
{
    [SerializeField] GameObject MainPage;
    [SerializeField] GameObject CreditPage;
    GameObject currentPage;

    // Start is called before the first frame update
    void Start()
    {
        MainPage.SetActive(true);
        if (CreditPage.activeSelf)
        {
            CreditPage.SetActive(false);
        }
    }

    //Go to the main game
    public void StartGame()
    {
        SceneManager.LoadScene("MainGame", LoadSceneMode.Single);
    }

    //open the credit page
    public void OpenCreditPage()
    {
        MainPage.SetActive(false);
        CreditPage.SetActive(true);
        currentPage = CreditPage;
    }

    //for back button in both credit and setting page
    public void BackBtn()
    {
        if (currentPage != null)
        {
            currentPage.SetActive(false);
            MainPage.SetActive(true);
            currentPage = null;
        }
    }

    //exit the game completely
    public void Exit()
    {
        Application.Quit();
    }
}
