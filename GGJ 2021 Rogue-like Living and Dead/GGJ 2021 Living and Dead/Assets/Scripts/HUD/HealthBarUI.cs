using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour
{
    [SerializeField] private Slider healthBar;
    // Start is called before the first frame update
    void Start()
    {//initialize the health bar UI
        healthBar.value = 1;
    }

    /*
     * Take the HP as a fraction between 0 and 1
     */
    public void updateUI(float hp)
    {
        healthBar.value = hp;
    }
}
