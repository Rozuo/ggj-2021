using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    public int damagePerTick = 1;
    public float tickInterval = 1f;

    private float lastTick = 0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null && collision.gameObject.tag == "Player")
        {
            if (Time.time - lastTick >= tickInterval)
            {
                collision.GetComponent<CharacterController>().Damage(damagePerTick);
                lastTick = Time.time;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision != null && collision.gameObject.tag == "Player")
        {
            if(Time.time - lastTick >= tickInterval)
            {
                collision.GetComponent<CharacterController>().Damage(damagePerTick);
                lastTick = Time.time;
            }
        }
    }
}
