using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

[RequireComponent(typeof(DunGen.Tile))]
[RequireComponent(typeof(Reskinner))]
public class RoomData : MonoBehaviour
{
    private Reskinner reskinner;
    private MapManager.WorldState currentState = MapManager.WorldState.LIVING;
    private DunGen.Tile tile;

    public int MinSpawns = 3;
    public int MaxSpawns = 6;
    public int NumWaves = 1;

    [SerializeField]
    private GameObject[] enemyPrefabs;

    [SerializeField]
    private GameObject[] undeadPrefabs;

    List<EnemyController> enemyList = new List<EnemyController>();
    List<EnemyController> undeadEnemyList = new List<EnemyController>();
    private int killedEnemies = 0;
    private int killedUndead = 0;

    private bool spawningUndead = false;
    private bool visited = false;

    private void Awake()
    {
        reskinner = GetComponent<Reskinner>();
        tile = GetComponent<DunGen.Tile>();
    }

    private void Start()
    {
        if(!visited) SetMapVisible(false);
        SetDoors(true);
        SetTriggersActive(true);
    }

    private void OnEnable()
    {
        MapManager.Instance.OnWorldStateChanged += OnWorldStateChanged;
    }

    private void OnDisable()
    {
        MapManager.Instance.OnWorldStateChanged -= OnWorldStateChanged;
    }

    private async void OnWorldStateChanged(MapManager.WorldState newState)
    {
        if (CameraTransition.Instance.CurrentTile == tile)
        {
            SetSkin(newState);

            if (newState == MapManager.WorldState.DEAD)
            {
                SetEnemiesActive(false);
                StartUndeadSpawnRoutine();
            }
            else
            {
                DestroyUndead();
                await Task.Delay(1000);
                SetEnemiesActive(true);
            }
        }
    }

    public void SetSkin(MapManager.WorldState state)
    {
        if (currentState != state)
        {
            reskinner.SetSkin(state);
            currentState = state;
        }
    }

    public SpawnPoint[] GetSpawnPoints()
    {
        return GetComponentsInChildren<SpawnPoint>();
    }

    public void SpawnEnemies()
    {
        if (MaxSpawns <= 0 || NumWaves <= 0) return;

        List<SpawnPoint> spawns = new List<SpawnPoint>(GetSpawnPoints());
        Extensions.Shuffle(spawns);

        int enemiesPRoom = Random.Range(MinSpawns, MaxSpawns);
        if (enemiesPRoom > 0)
        {
            SetDoors(false);
            Debug.Log($"Spawning {enemiesPRoom} enemies");
            for (int j = 0; j < enemiesPRoom; j++)
            {
                GameObject gO = Instantiate(enemyPrefabs[0], spawns[j].transform.position, Quaternion.identity);
                EnemyController enemy = gO.GetComponent<EnemyController>();
                enemyList.Add(enemy);
            }
        }

        NumWaves -= 1;
    }

    public void spawnUndead(int num)
    {
        Debug.Log($"Spawning {num} undead");

        List<SpawnPoint> spawns = new List<SpawnPoint>(GetSpawnPoints());

        for (int i = 0; i < num; i++)
        {
            GameObject gO = Instantiate(undeadPrefabs[0], spawns[Random.Range(0, spawns.Count - 1)].transform.position, Quaternion.identity);
            EnemyController ec = gO.GetComponent<EnemyController>();
            ec.MakeUndead();
            undeadEnemyList.Add(ec);
        }
    }


    public void KilledEnemy(EnemyController enemy) // could add enemy type in the parameters for varity.
    {
        killedEnemies += 1;
        if (enemyList.Contains(enemy))
        {
            enemyList.Remove(enemy);
        }
        Debug.Log($"Killed enemy. {enemyList.Count} remaining");

        if(enemyList.Count <= 0)
        {
            SetDoors(true);
        }
    }

    public void KilledUndead(EnemyController enemy)
    {
        if (undeadEnemyList.Contains(enemy))
        {
            undeadEnemyList.Remove(enemy);
        }
        if (currentState == MapManager.WorldState.DEAD)
        {
            killedUndead += 1;
            if (killedUndead >= killedEnemies)
            {
                Debug.Log($"Killed undead. Reviving player");
                GameManager.Instance.PlayerRevived();
            }
            else
            {
                Debug.Log($"Killed undead. {killedEnemies} remaining");
            }
        }
    }

    public void StartRoom()
    {
        if(!visited)
        {
            SetMapVisible(true);
            visited = true;            
        }
        if (currentState == MapManager.WorldState.LIVING)
        {
            if (enemyList.Count == 0)
            {
                if (NumWaves > 0) SpawnEnemies();
                else
                {
                    SetTriggersActive(false);
                }
            }
            else
            {
                SetEnemiesActive(true);
            }
        }
        else
        {
            StartUndeadSpawnRoutine();
        }
    }

    public void RoomExited()
    {
        SetEnemiesActive(false);
        DestroyUndead();
    }

    public void SetEnemiesActive(bool active)
    {
        foreach (EnemyController ec in enemyList)
        {
            ec.gameObject.SetActive(active);
        }
    }

    public void DestroyUndead()
    {
        spawningUndead = false;
        for (int i = 0; i < undeadEnemyList.Count; i++)
        {
            if (undeadEnemyList[i] != null && undeadEnemyList[i].gameObject != null)
            {
                Destroy(undeadEnemyList[i].gameObject);
            }
        }
        undeadEnemyList.Clear();
    }

    private async void StartUndeadSpawnRoutine()
    {
        await Task.Delay(1000);
        spawningUndead = true;
        while (spawningUndead)
        {
            if (undeadEnemyList.Count < killedEnemies + 1)
            {
                spawnUndead(1);
            }

            await Task.Delay(1000);
        }
    }

    private void SetMapVisible(bool visible)
    {
        MapHUDElement[] mapPieces = GetComponentsInChildren<MapHUDElement>(true);
        foreach (MapHUDElement piece in mapPieces)
        {
            piece.gameObject.SetActive(visible);
        }
    }

    private void SetDoors(bool Open)
    {
        RoomDoor[] Doors = GetComponentsInChildren<RoomDoor>(true);
        foreach (RoomDoor door in Doors)
        {
            door.gameObject.SetActive(!Open);
        }
        if(!Open) SetTriggersActive(false);
    }

    private void SetTriggersActive(bool active)
    {
        StartTrigger[] triggers = GetComponentsInChildren<StartTrigger>();
        foreach (StartTrigger trigger in triggers)
        {
            trigger.gameObject.SetActive(active);
        }
    }
}
