using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public bool isEnemySpawn = true;
    public bool isPlayerSpawn = false;

    private void Awake()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        if(renderer != null)
        {
            renderer.enabled = false;
        }
    }
}
