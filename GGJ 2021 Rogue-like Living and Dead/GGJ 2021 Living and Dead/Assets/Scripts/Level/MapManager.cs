using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapManager : MonoBehaviour
{
    public delegate void WorldStateChangedEvent(WorldState newState);

    public WorldStateChangedEvent OnWorldStateChanged;
    public enum WorldState
    {
        LIVING,
        DEAD
    }

    public WorldState CurrentState = WorldState.LIVING;

    [Serializable]
    public class SpriteSkin
    {
        public WorldState state;
        public Sprite[] sprites;
    }

    [SerializeField] private SpriteSkin[] Skins;

    public static MapManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    PlayerDied();
        //}
        //if (Input.GetKeyDown(KeyCode.O))
        //{
        //    PlayerResurrected();
        //}
    }

    public void PlayerDied()
    {
        Debug.Log("Setting world to DEAD");
        CurrentState = WorldState.DEAD;
        PlayerHUD.Instance.DoCameraFlash(Color.red, 1f);
        OnWorldStateChanged?.Invoke(CurrentState);
    }

    public void PlayerResurrected()
    {
        Debug.Log("Setting world to LIVING");
        CurrentState = WorldState.LIVING;
        PlayerHUD.Instance.DoCameraFlash(Color.white, 1f);
        OnWorldStateChanged?.Invoke(CurrentState);
    }

    public SpriteSkin GetSkin(WorldState state)
    {
        foreach (SpriteSkin skin in Skins)
        {
            if (skin.state == state)
            {
                return skin;
            }
        }
        return null;
    }
}
