using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;

public class Reskinner : MonoBehaviour
{
    private List<Tilemap> tilemaps = new List<Tilemap>();
    private List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

    public void SetSkin(MapManager.WorldState state)
    {
        RefreshRenderers();
        foreach (Tilemap tilemap in tilemaps)
        {
            for (int x = (int)tilemap.localBounds.min.x; x < tilemap.localBounds.max.x; x++)
            {
                for (int y = (int)tilemap.localBounds.min.y; y < tilemap.localBounds.max.y; y++)
                {
                    Vector3Int tilePos = new Vector3Int(x, y, 0);
                    TileBase tb = tilemap.GetTile(tilePos);

                    ReskinnableTileBase rtb = (ReskinnableTileBase)ScriptableObject.CreateInstance(typeof(ReskinnableTileBase));

                    if (tb == null || rtb == null || tb.name.Length < 1)
                    {
                        continue;
                    }

                    Sprite replace = getSubSpriteBySkin(state, tb.name);
                    if (replace != null)
                    {
                        rtb.sprite = replace;
                        rtb.name = tb.name;

                        tilemap.SwapTile(tb, (TileBase)rtb);
                    }
                }
            }
        }

        foreach (SpriteRenderer renderer in spriteRenderers)
        {
            Sprite replace = getSubSpriteBySkin(state, renderer.sprite.name);
            if(replace != null)
            {
                renderer.sprite = replace;
            }
        }
    }

    public void RefreshRenderers()
    {
        tilemaps.Clear();
        spriteRenderers.Clear();
        foreach (Tilemap tm in GetComponentsInChildren<Tilemap>())
        {
            if (tm.GetComponent<TilemapCollider2D>() == null)
            {
                tilemaps.Add(tm);
            }
        }
        spriteRenderers.AddRange(GetComponentsInChildren<SpriteRenderer>());
    }

    Sprite getSubSpriteBySkin(MapManager.WorldState state, string spriteName)
    {
        MapManager.SpriteSkin skin = MapManager.Instance.GetSkin(state);
        if (skin != null)
        {
            foreach (Sprite s in skin.sprites)
            {
                if (s.name == spriteName) return s;
            }
        }
        return null;
    }
}