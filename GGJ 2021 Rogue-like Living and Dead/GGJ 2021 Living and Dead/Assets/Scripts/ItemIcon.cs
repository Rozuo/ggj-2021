using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemIcon : MonoBehaviour
{
    private Text text;
    private Image image;

    // Get references
    void Start()
    {       
        text = gameObject.transform.Find("ItemQuantity").GetComponent<Text>();
        image = gameObject.GetComponent<Image>();
    }

    public void SetText(int newTxt)
    {
        text.text = "x" + newTxt;
    }

    public void SetImage(Sprite newImg)
    {        
        image.sprite = newImg;
    }
}
