using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundtrackController : MonoBehaviour
{
    [SerializeField] GameObject overworldMusic;
    [SerializeField] GameObject underworldMusic;

    AudioSource Overworld { get { return overworldMusic.GetComponent<AudioSource>(); } }
    AudioSource Underworld { get { return underworldMusic.GetComponent<AudioSource>(); } }

    public void EnteredOverworld()
    {
        Underworld.Stop();
        Overworld.Play();
    }

    public void EnteredUnderworld()
    {
        Overworld.Stop();
        Underworld.Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        Overworld.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
