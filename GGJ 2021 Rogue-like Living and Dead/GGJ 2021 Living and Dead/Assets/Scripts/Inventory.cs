using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public Dictionary<Item, int> inventory;
    public ItemUI invUI;

    // Start is called before the first frame update
    void Awake()
    {
        inventory = new Dictionary<Item, int>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddItem(Item itm, int quantity)
    {
        if (inventory.TryGetValue(itm, out int value))
            inventory[itm] += quantity;
        else
            inventory.Add(itm, quantity);

        //GetInventoryString();
    }

    public int GetCount()
    {
        return inventory.Count;
    }

    private void GetInventoryString()
    {
        string invString = "";
        foreach(KeyValuePair<Item, int> invItem in inventory)
        {
            invString += invItem.Key.itemName + " | " + invItem.Value + "\n";           
        }
        Debug.Log(invString);
    }
}
