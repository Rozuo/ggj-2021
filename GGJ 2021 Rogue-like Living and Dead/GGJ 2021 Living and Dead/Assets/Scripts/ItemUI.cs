using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    public GameObject uiObj;
    public GameObject uiDesc;
    public int xoffset = 32;
    private List<ItemIcon> itemList;
    private Inventory invObj;
    private IEnumerator timeCoroutine;

    [ContextMenu("Refresh Inventory UI")]
    void ContextOption()
    {
        RefreshInventory();
    }

    // Get references
    void Awake()
    {       
        if (!GameObject.Find("Inventory"))
        {
            Debug.LogError(gameObject.name + " failed to find the Inventory object. Make sure to add the Inventory prefab to your scene!");
            gameObject.SetActive(false);
        }
        else
        {
            invObj = GameObject.Find("Inventory").GetComponent<Inventory>();
            invObj.invUI = gameObject.GetComponent<ItemUI>();
        }

        itemList = new List<ItemIcon>();
      
        for(int i = 0; i < 15; i++)
        {
            GameObject newSlot = Instantiate(uiObj, new Vector3((xoffset*i), 0, 0), Quaternion.identity);
            newSlot.transform.SetParent(gameObject.transform, false);           
            itemList.Add(newSlot.GetComponent<ItemIcon>());           
        }
    }   

    public void RefreshInventory()
    {
        foreach(ItemIcon i in itemList)
        {
            i.gameObject.SetActive(false);
        }

        // TODO: Include a flexible method to add more item slots.
        int index = 0;
        foreach (KeyValuePair<Item, int> invItem in invObj.inventory)
        {
            itemList[index].gameObject.SetActive(true);
            itemList[index].SetImage(invItem.Key.itemImage);
            itemList[index].SetText(invItem.Value);
            index++;
        }
    }

    public void SetTimedDescription(string newMsg)
    {
        uiDesc.GetComponent<CanvasGroup>().alpha = 1;
        uiDesc.transform.GetChild(0).GetComponent<Text>().text = newMsg;
        
        if (timeCoroutine != null)
            StopCoroutine(timeCoroutine);

        timeCoroutine = OnDescriptionTimer(3);
        StartCoroutine(timeCoroutine);
    }

    private IEnumerator OnDescriptionTimer(float time)
    {
        yield return new WaitForSeconds(time);
        uiDesc.transform.GetChild(0).GetComponent<Text>().text = "";
        uiDesc.GetComponent<CanvasGroup>().alpha = 0;
    }
}
