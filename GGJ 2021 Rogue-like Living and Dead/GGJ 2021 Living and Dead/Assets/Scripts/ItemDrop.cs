using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Using the Drop Table:
 * 1. Add Prefab Items to the dropItem list.
 * 2. Add integer drop chance (1-100) for each item.
 */
public class ItemDrop : MonoBehaviour
{
    [Tooltip("If enabled, allows multiple items to drop in a single roll.")]
    public bool dropMultiple = true;
    [Tooltip("If enabled, rolls random again for each item.")]
    public bool separateRolls = true;
    public List<GameObject> dropItem;
    public List<int> dropChance;
    
    [ContextMenu("Roll the Drop Chance")]
    void ContextOption()
    {
        DropItems();
    }

    public void DropItems()
    {
        //roll a random number 1-100
        int number = Random.Range(0, 100);
        //Debug.Log("[rng number: " + number + "]");

        //compare value to table
        for (int i = 0; i < dropItem.Count; i++)
        {          
            if(i >= dropChance.Count)
            {
                Debug.LogWarning("An item is missing their drop chance.");
                continue;
            }
  
            if (number < dropChance[i])
            {
                //Debug.Log("Item Dropped: " + dropItem[i].GetComponent<ItemObject>().itemid.itemName);
                GameObject newItem = Instantiate(dropItem[i]);
                Vector3 newPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -0.05f);
                newItem.transform.position = newPos; // gameObject.transform.position;

                if (!dropMultiple)
                    break;
            }

            // re-roll the number
            if (separateRolls)
            {
                number = Random.Range(0, 100);
                //Debug.Log("[reroll rng number: " + number + "]");
            }
        }
    }
}
