using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    [SerializeField] GameObject psiBall;
    [SerializeField] GameObject girlBall;
    [SerializeField] GameObject gokuBall;
    [SerializeField] GameObject footsteps;
    [SerializeField] GameObject deathWhoosh;
    [SerializeField] GameObject reviveWhoosh;

    public AudioSource PsiBallSound { get { return psiBall.GetComponent<AudioSource>(); } }
    public AudioSource GirlBallSound { get { return girlBall.GetComponent<AudioSource>(); } }
    public AudioSource GokuBallSound { get { return gokuBall.GetComponent<AudioSource>(); } }
    public AudioSource FootstepsSound { get { return footsteps.GetComponent<AudioSource>(); } }
    public AudioSource DeathWhooshSound { get { return deathWhoosh.GetComponent<AudioSource>(); } }
    public AudioSource ReviveWhooshSound { get { return reviveWhoosh.GetComponent<AudioSource>(); } }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
