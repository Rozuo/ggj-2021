﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaiveEnemyAI : EnemyAI
{
    public override Directive GetDirective()
    {
        var dist = player.transform.position - enemy.transform.position;

        if (dist.magnitude < 3)
        {
            return new Directive()
            {
                ShootTarget = new Vector2(player.transform.position.x, player.transform.position.y)
            };
        }
        else if (dist.magnitude < 5)
        {
            return new Directive()
            {
                MoveTarget = new Vector2(player.transform.position.x, player.transform.position.y),
                ShootTarget = new Vector2(player.transform.position.x, player.transform.position.y)
            };
        }
        else
        {
            return new Directive()
            {
                MoveTarget = new Vector2(player.transform.position.x, player.transform.position.y)
            };
        }
    }
}