﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Directive
{
    public Vector2? MoveTarget;
    public Vector2? ShootTarget;
}
