﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAI
{
    public GameObject player;
    public GameObject enemy;

    protected PlayerController PlayerController { get { return player.GetComponent<PlayerController>(); } }
    protected EnemyController EnemyController { get { return enemy.GetComponent<EnemyController>(); } }

    public abstract Directive GetDirective();
}
