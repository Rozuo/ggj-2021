using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        Start, Play, Pause, Transition, GameOver
    }

    public delegate void GameStateChangedEvent(GameState newState);
    public GameStateChangedEvent OnGameStateChanged;

    [SerializeField]
    private GameObject runTimeDungeon;

    [SerializeField] GameObject soundtrack;
    SoundtrackController Soundtrack { get { return soundtrack.GetComponent<SoundtrackController>(); } }

    [SerializeField] GameObject sfx;
    SFXController SFX { get { return sfx.GetComponent<SFXController>(); } }

    private CameraTransition camTrans { get { return CameraTransition.Instance; } }
    private MapManager mapManager { get { return MapManager.Instance; } }

    [SerializeField] GameObject playerPrefab;
    private PlayerController playersController;

    private bool playerDead = false;

    public GameState CurrentState { get; private set; } 

    public RoomData CurrentRoom { get { return camTrans.CurrentTile.GetComponent<RoomData>(); } }

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        playerPrefab = GameObject.FindGameObjectWithTag("Player");
        playersController = playerPrefab.GetComponent<PlayerController>();
        Debug.Log(mapManager);
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentState)
        {
            case GameState.Start:
                break;
            case GameState.Play:
                if (Input.GetButtonDown("Pause"))
                {
                    PlayerHUD.Instance.ShowMap(true);
                    Time.timeScale = 0;
                    SetState(GameState.Pause);
                }
                break;
            case GameState.Pause:
                if (Input.GetButtonDown("Pause"))
                {
                    PlayerHUD.Instance.ShowMap(false);
                    Time.timeScale = 1;
                    SetState(GameState.Play);
                }
                break;
        }
    }

    public void SetState(GameState newState)
    {
        CurrentState = newState;
        OnGameStateChanged?.Invoke(newState);
    }

    public void PlayerDied()
    {
        mapManager.PlayerDied();
        Soundtrack.EnteredUnderworld();
        SFX.DeathWhooshSound.Play();
    }

    public void PlayerRevived()
    {
        playersController.Heal(100);
        playersController.Revived();
        playersController.ZeroForce();
        mapManager.PlayerResurrected();
        Soundtrack.EnteredOverworld();
        SFX.ReviveWhooshSound.Play();
    }

    public void PlayerPermaDeath()
    {
        playerDead = true;
        SceneManager.LoadScene("Game Over");
        //show game over screen
        //restart game
        //etc.
    }

    public void KilledEnemy(EnemyController enemy) // could add enemy type in the parameters for varity.
    {
        CurrentRoom.KilledEnemy(enemy);
    }

    public void KilledUndead(EnemyController enemy)
    {
        CurrentRoom.KilledUndead(enemy);
    }
}
